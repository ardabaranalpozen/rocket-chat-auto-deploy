# rocket-chat-auto-deploy

This project is prepared to deploy the Rocket Chat application on AKS by creating AKS (Azure Kubernetes Service).The technologies and requirements used are listed below.

![](rocketchatdeploy.png)

# Prerequisites


1. Azure Subscription
2. Azure Service Principal Credentials
   
   You can get Azure Service Principal by running the following commands in Azure CLI.

   Get Subscription ID:  az account list |  grep -oP '(?<="id": ")[^"]*'
   
   Create SP for Subscription:  
            az ad sp create-for-rbac \
            --role="Contributor" \
            --scopes="/subscriptions/SUBSCRIPTION_ID"
   WARNING: Note the output of this command!

# How To Use?

You can use the GUI given below to use this automation. Only people connected to the Private Network can access the GUI. You can create your own environment by entering the credentials of your own Azure account.


# Technology Stack
1. Terraform: AKS installation on the Azure platform is done with Terraform.
2. Kubernetes: Applications run on Kubernetes. Deployment is done with kubernetes yaml files.
3. Azure CLI: It is used to create the Service Principal on Azure and to manage the Azure platform.
4. GitLab: The project repository is maintained on GitLab.
5. GitLab Runner: It is used to run the pipeline and make automatic deployment. In this project, gitlab runner works as a docker container. When Runner runs, it creates a container with the image below and runs the commands in it. This image was prepared by me and it includes terraform, azure cli and kubectl.
          
          Docker Image : ardabaran/aks-deployer:latest

6. JavaScript: Automation GUI has been prepared by JavaScript.

# Pipeline Steps

1.  Create AKS: In this step, AKS is installed with Terraform commands. Parameters required for installation are given as variables. Required variables should be given when triggering the pipeline for different installations.The required list of variables is as follows.

        - ARM_CLIENT_ID : Azure Service Principal Client Id
        - ARM_CLIENT_SECRET: Azure Service Principal Client Secret
        - ARM_SUBSCRIPTION_ID: Azure Subscription Id
        - ARM_TENANT_ID: Azure Service Principal Tenant Id

    
Terraform variables are also used in this step. The following variables determine the characteristics of the AKS to be installed.

        - TF_VAR_RG_NAME : AKS Resource Group Name
        - TF_VAR_CLUSTER_NAME : AKS Cluster Name
        - TF_VAR_LOCATION : Location where AKS will be installed


2.  Deploy Apps: In this step, kubernetes yaml files are deployed to AKS created in the previous step via "kubectl" command. The outputs.tf file provides us the required kubeconfig file after AKS deployment. Basically, the following commands run in this step.

        - kubectl apply -f ./kubernetes_files/rocketchat-mongo.yaml    --> Deploy Mongo DB(required for rocket chat)
        - kubectl apply -f ./kubernetes_files/rocketchat-app.yaml      --> Deploy Rocket Chat App


3.  App Test: In this step, it is checked whether the deployed Rocket Chat application is live. Testing is done in this step with a simple REST API request.

4. Destroy: In this step, all the resources created before are deleted from the Azure platform. This step is defined as manual trigger. This step can be run manually when AKS is desired to be deleted.

# Automation GUI

A small GUI is designed for this automation. With this GUI designed with Javascript, the variables required for the Pipeline are given and the pipeline is triggered. Triggering is done using the GitLab Pipeline API. (This GUI works in Private Network environment. Only Private Network connected users can access this link.)


![](gui.png)

