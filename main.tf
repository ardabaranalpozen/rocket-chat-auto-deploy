resource "azurerm_resource_group" "rg" {
  name     = var.RG_NAME
  location = var.LOCATION
}

resource "azurerm_kubernetes_cluster" "cluster" {
  name                = var.RG_CLUSTER_NAME
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  dns_prefix          = var.RG_CLUSTER_NAME

  default_node_pool {
    name       = "default"
    node_count = "2"
    vm_size    = "standard_d2_v2"
  }

  identity {
    type = "SystemAssigned"
  }
}
