provider "azurerm" {
  features {}
  subscription_id = var.SUBSCRIPTION_ID
}

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.48.0"
    }
  }
}

