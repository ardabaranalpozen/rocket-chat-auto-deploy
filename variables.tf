variable "RG_NAME" {
  default = "aks-resource-group"
}

variable "RG_CLUSTER_NAME" {
  default = "aks-cluster"
}

variable "LOCATION" {
  default = "France Central"
}

variable "SUBSCRIPTION_ID" {
  type = string
}
